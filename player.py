import pygame, sys, os, pdb, registry
from player_components import PlayerGraphics_, PlayerPhysics_, PlayerEvents_

class Player(object):
    def __init__(self):
        self.x = registry.screen_size[0]/2
        self.y = registry.screen_size[1]/2
        self.image = pygame.image.load("items/player.png")
        
        self.init_components()

    def init_components(self):
        self.graphics_ = PlayerGraphics_(self)

    def update(self):
        self.graphics_.update()

        








