import pygame, sys, os, pdb, registry
from pygame.locals import *
from base_component import Component

class WorldInit_(Component):
    def default_spawn(self):
        player_rect = registry.player.image.get_rect()
        tile_size = self.parent.tile_size
        initial_x = registry.screen_size[0]/2
        initial_y = registry.screen_size[1]/2
        self.parent.x = 0 
        self.parent.y = 0
        self.parent.initial_x = initial_x - (self.parent.spawn_point[0] * tile_size) - (tile_size - player_rect.width)/2
        self.parent.initial_y = initial_y - (self.parent.spawn_point[1] * tile_size) - (tile_size - player_rect.height)/2

    def spawn(self):
        if self.parent.spawn_trigger == True:
            self.default_spawn()
            self.parent.spawn_trigger = False

    def update(self):
        self.spawn()

class WorldGraphics_(Component):
    def __init__(self, parent):
        self.parent = parent
        self.line_width = 1
        self.limit = 20

    def draw(self):
        self.point_list = [
            (self.parent.x + self.parent.initial_x, self.parent.y + self.parent.initial_y),
            (self.parent.x + self.parent.initial_x, self.parent.y + self.parent.height + self.parent.initial_y),
            (self.parent.x + self.parent.width + self.parent.initial_x, self.parent.y + self.parent.height + self.parent.initial_y),
            (self.parent.x + self.parent.width + self.parent.initial_x, self.parent.y + self.parent.initial_y),
            (self.parent.x + self.parent.initial_x, self.parent.y + self.parent.initial_y)
        ]
        self.rect = pygame.draw.lines(registry.screen, (255,255,0), False, self.point_list, self.line_width)

    def update(self):
        self.draw()

class WorldEvents_(Component):
    def __init__(self, parent):
        self.parent = parent
        self.move_up = False
        self.move_down = False
        self.move_right = False
        self.move_left = False
        self.grid_on = True
        
    def update(self, event):
        if event.type == KEYDOWN:
            if event.key == K_LSHIFT: self.parent.move_speed = 5
            if event.key == K_w: self.move_up = True
            if event.key == K_s: self.move_down = True
            if event.key == K_d: self.move_right = True
            if event.key == K_a: self.move_left = True
            if event.key == K_g: self.grid_on = not self.grid_on

        if event.type == KEYUP:
            if event.key == K_LSHIFT: self.parent.move_speed = 1
            if event.key == K_w: self.move_up = False
            if event.key == K_s: self.move_down = False
            if event.key == K_d: self.move_right = False
            if event.key == K_a: self.move_left = False

class WorldPhysics_(Component):
    def move(self):
        events_ = self.parent.events_
        if events_.move_down == True:
            self.parent.y = self.parent.y - self.parent.move_speed
        if events_.move_up == True:
            self.parent.y = self.parent.y + self.parent.move_speed
        if events_.move_right == True:
            self.parent.x = self.parent.x - self.parent.move_speed
        if events_.move_left == True:
            self.parent.x = self.parent.x + self.parent.move_speed

    def update(self):
        self.move()

class WorldCollision_(Component):

    def collision(self):
        image_rect = registry.player.image_rect
        x, y = self.parent.x, self.parent.y
        initial_x, initial_y = self.parent.initial_x, self.parent.initial_y
        height, width = self.parent.height, self.parent.width

        if image_rect.left < x + initial_x:
            self.parent.x = image_rect.left - initial_x
        if image_rect.right >= x + width + initial_x:
            self.parent.x = image_rect.right - width - initial_x
        if image_rect.top < y + initial_y:
            self.parent.y = image_rect.top - initial_y
        if image_rect.bottom >= y + height + initial_y:
            self.parent.y = image_rect.bottom - height - initial_y

    def update(self):
        self.collision()

class WorldGrid_(Component):
    def __init__(self, parent):
        self.parent = parent
        self.rgb = (255,255,255)
 
    def draw(self):
        initial_x, initial_y = self.parent.initial_x, self.parent.initial_y
        x, y = self.parent.x, self.parent.y
        height, width = self.parent.height, self.parent.width

        for column in range(self.parent.columns + 1):
            x1, y1 = self.parent.tile_size*column + (initial_x + x), initial_y + y 
            x2, y2 = self.parent.tile_size*column + (initial_x + x), initial_y + height + y
            pygame.draw.line(registry.screen, (self.rgb), (x1, y1), (x2, y2))

        for row in range(self.parent.rows + 1):
            x1, y1 = initial_x + x, self.parent.tile_size*row + (initial_y + y)
            x2, y2 = initial_x + x + width, self.parent.tile_size*row + (initial_y + y)
            pygame.draw.line(registry.screen, (self.rgb), (x1, y1), (x2, y2))

    def update(self):
        if self.parent.events_.grid_on == True:
            self.draw()     

class WorldDoor_(Component):
    def __init__(self, parent, x, y):
        self.x = x
        self.y = y
        self.image = pygame.image.load("items/door.png")
    
    def draw(self):
        registry.screen.blit(self.image, (self.x, self.y))

    def check_activity(self):
        pass

    def activate(self):
        pass 

    def update(self):
        self.draw()


    
