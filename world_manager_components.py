import pygame, sys, os, pdb, math, registry
from pygame.locals import *
from base_component import Component
from itertools import cycle

class WorldManagerInit_(Component):
    def __init__(self, parent, world_list):
        self.parent = parent
        self.world_list = world_list
        self.init_worlds()
        self.set_active_world("home")

    def init_worlds(self):
        for world in self.world_list:
            self.parent.modules.append(world())

    def set_active_world(self, world_id):
        for world in self.parent.modules:
            if world.id == world_id:
                self.parent.active_world = world
                self.parent.active_world.spawn_trigger = True

    def update(self):
        self.parent.active_world.update()

class WorldManagerEvents_(Component):   
    def __init__(self, parent):
        self.parent = parent
        self.next_world = False
    
    def events(self, event):
        if event.type == KEYDOWN:
            if event.key == K_t: self.next_world = True
    
    def update(self, event):
        self.events(event)
        self.parent.active_world = self.parent.active_world
        self.parent.active_world.events_.update(event)

class WorldManagerCycle_(Component):
    def cycle_world(self):
        if self.parent.events_.next_world == True:
            index = self.parent.modules.index(self.parent.active_world)
            try:
                self.parent.active_world = self.parent.modules[index + 1]
            except IndexError: 
                self.parent.active_world = self.parent.modules[0]
            self.parent.active_world.spawn_trigger = True
            self.parent.events_.next_world = False
    
    def update(self):
        self.cycle_world()

