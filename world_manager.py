import pygame, sys, os, pdb, math, registry
from home import Home
from level1 import Level1
from level2 import Level2
from world_manager_components import WorldManagerEvents_, WorldManagerCycle_, WorldManagerInit_

class WorldManager(object):
    def __init__(self):
        self.modules = []
        self.world_list = [Home, Level1, Level2]
        self.init_components()

    def init_components(self):
        self.init_ = WorldManagerInit_(self, self.world_list)
        self.events_ = WorldManagerEvents_(self)
        self.cycle_ = WorldManagerCycle_(self)

    def update(self):       
        self.init_.update()
        self.cycle_.update()
