import pygame, sys, os, pdb, registry
from pygame.locals import *
from base_component import Component

class PlayerGraphics_(Component):
    def draw(self):
        self.parent.image_rect = registry.screen.blit(self.parent.image, (self.parent.x, self.parent.y))

    def update(self):
        self.draw()

class PlayerEvents_(Component):
    def __init__(self, parent):
        self.parent = parent
        self.move_up = False
        self.move_down = False
        self.move_right = False
        self.move_left = False

    def update(self, event):
        if event.type == KEYDOWN:
            if event.key == K_LSHIFT: self.parent.move_speed = 5
            if event.key == K_w: self.move_up = True
            if event.key == K_s: self.move_down = True
            if event.key == K_d: self.move_right = True
            if event.key == K_a: self.move_left = True

        if event.type == KEYUP:
            if event.key == K_LSHIFT: self.parent.move_speed = 1
            if event.key == K_w: self.move_up = False
            if event.key == K_s: self.move_down = False
            if event.key == K_d: self.move_right = False
            if event.key == K_a: self.move_left = False

class PlayerPhysics_(Component):
    
    def move(self):
        events_ = self.parent.events_
        if events_.move_up == True:
            self.parent.y = self.parent.y - self.parent.move_speed
        if events_.move_down == True:
            self.parent.y = self.parent.y + self.parent.move_speed
        if events_.move_right == True:
            self.parent.x = self.parent.x + self.parent.move_speed
        if events_.move_left == True:
            self.parent.x = self.parent.x - self.parent.move_speed

    def update(self):
        self.move()