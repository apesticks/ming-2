import pygame, sys, os, pdb, registry
from pygame.locals import *
    
pygame.init()
fps = pygame.time.Clock()
running = True  

def run():
    while running:
        fps.tick(60)
        registry.screen.fill((0,0,0))
        update()
        events()
        pygame.display.update()
 
def events():
    for event in pygame.event.get():
        game_quit_events(event)
        for module in registry.modules:
            if hasattr(module, "events_"):
                module.events_.update(event)

def game_quit_events(event):
    global running
    if event.type == QUIT:
        running = False
    if event.type == KEYDOWN:
       if event.key == K_ESCAPE:
           running = False

def update():
    for module in registry.modules:
        module.update()

if __name__ == "__main__":
    run()