import pygame, sys, os, pdb
from world import World

class Level2(World):
    def __init__(self):
        self.columns = 10
        self.rows = 10
        self.id = "level2"
        self.spawn_point = 2, 2
        super(Level2, self).__init__()