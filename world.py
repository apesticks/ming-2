import pygame, sys, os, pdb, registry
from world_components import WorldGraphics_, WorldEvents_, WorldPhysics_, WorldGrid_, WorldCollision_, WorldInit_, WorldDoor_

class World(object):
    def __init__(self):
        self.tile_size = 40
        self.move_speed = 1
        self.spawn_trigger = False
        self.x = 0
        self.y = 0
        self.width = self.columns * self.tile_size
        self.height = self.rows * self.tile_size
        self.initial_x = registry.screen_size[0]/2
        self.initial_y = registry.screen_size[1]/2
        
        self.init_components()

    def init_components(self):
        self.init_ = WorldInit_(self)
        self.graphics_ = WorldGraphics_(self)
        self.physics_ = WorldPhysics_(self)
        self.events_ = WorldEvents_(self)
        self.collision_ = WorldCollision_(self)
        self.door_ = WorldDoor_(self, 50, 50)
        self.grid_ = WorldGrid_(self)

    def update(self):
        self.init_.update()
        self.graphics_.update()
        self.physics_.update()
        self.collision_.update()
        self.door_.update()
        self.grid_.update()

