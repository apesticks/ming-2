import pygame, sys, os, pdb
from world import World

class Level1(World):
    def __init__(self):
        self.columns = 5
        self.rows = 5
        self.id = "level1"
        self.spawn_point = 1, 1
        super(Level1, self).__init__()