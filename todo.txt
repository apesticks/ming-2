The project:
    minor character story
        - giving the player a minor idea of why they are in the world + objective

    charaters starts in a home base
        - has very minimal attributes that can be interacted with
        - as you progress through the game you unlock more and more doorways
        - as you progress through the game you unlock interactive npc's or objects

    objective
        - to reach the last floor/last door/end boss and kill them to surve your purpose
        - must obtain a significant amount of good items with synergy to be able to complete
        - game is not meant to be easily defeated every run

    characters
        - 4+ character clases that can be unlocked through finishing the game

    end reward
        - unlock classes
        - unlock new items that can be found/used/created in the game
        - titled acomplishments

    failure
        - once dead you start all over again

    goal
        - a run of the game must not last more than 1 to 1.5 hours long
        - game should be as vague as possible in terms of its mechanics to encourage experamentation/exploration


Detailed objective:

    fighting styles
        - hand to hand
        - swords
        - ranged weapons
        - magic
    
    character
        - has stats such as:-
            .health, mana, energy/stamina, rage 
            .damage, move speed, atack speed, shield
        - has minimal inventory
            .can equip 1-3 trinkets
            .can carry consumable items
    





