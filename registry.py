import os, pygame
from player import Player
from world_manager import WorldManager
screen_size=(800,600)
window_position = 790, 250
os.environ["SDL_VIDEO_WINDOW_POS"] = str(window_position[0]) + "," + str(window_position[1])
screen = pygame.display.set_mode(screen_size, 0, 32)


player = Player()
world_manager = WorldManager()

modules = [
    player,
    world_manager,
]