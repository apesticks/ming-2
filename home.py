import pygame, sys, os, pdb
from world import World

class Home(World):
    def __init__(self):
        self.columns = 2
        self.rows = 2
        self.spawn_point = 0, 0
        self.id = "home"
        super(Home, self).__init__()
        